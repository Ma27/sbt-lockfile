package se.nullable.sbtix.lockfile

import coursier.Cache
import java.io.{File, IOException}
import java.nio.file.{Files, Path}
import java.security.MessageDigest
import sbt.io.Hash
import scala.collection.concurrent.TrieMap

class LockfileBuilder {
  private val files = TrieMap[Path, LockfileEntry]()
  private val inProgress = TrieMap[String, Path]()

  def build: Lockfile = {
    assert(inProgress.isEmpty, "There are still entries being downloaded!")
    Lockfile(files.toMap)
  }

  def addOrVerify(url: String, absolutePath: Path, cachePath: Path): Unit = {
    val sha256 = LockfileBuilder.hashSha256(absolutePath)
    files.get(absolutePath) match {
      case None =>
        files += cachePath.relativize(absolutePath) -> LockfileEntry(url,
                                                                     sha256)
      case Some(entry) =>
        LockfileBuilder.verify(entry, absolutePath)
    }
  }

  def logger(cache: Path, innerLogger: Cache.Logger) =
    new CoursierLoggerWrapper(innerLogger) {
      override def foundLocally(url: String, f: File): Unit = {
        super.foundLocally(url, f)
        addOrVerify(url, f.toPath(), cache)
      }
      override def downloadingArtifact(url: String, file: File): Unit = {
        super.downloadingArtifact(url, file)
        inProgress += url -> file.toPath
      }
      override def downloadedArtifact(url: String, success: Boolean): Unit = {
        super.downloadedArtifact(url, success)
        val file =
          inProgress
            .remove(url)
            .getOrElse(
              throw new IllegalArgumentException(
                s"Never started downloading $url"))
        if (success) {
          addOrVerify(url, file, cache)
        }
      }
    }
}

object LockfileBuilder {
  def hashSha256(path: Path): String = {
    val hasher = MessageDigest.getInstance("SHA-256")
    hasher.update(Files.readAllBytes(path))
    return Hash.toHex(hasher.digest())
  }

  def verify(entry: LockfileEntry, absolutePath: Path): Unit = {
    val sha256 = hashSha256(absolutePath)
    if (sha256 != entry.sha256) {
      throw new IOException(
        s"URL ${entry.sha256} ($absolutePath) is already locked to sha256:${entry.sha256}, but got sha256:$sha256")
    }
  }
}
